// Real world application of objects

// Object constructor

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.lvl = level;
    this.health = 2 * level;
    this.atk = level;

    // Methods
    this.tackle = function(target) {
        target.health = target.health - this.atk;
        console.log(`${this.name} tackled ${target.name}`);

        if (target.health <= 0) {
            target.faint()
        } else {
            console.log(`target_Pokemon's health is now reduced to`)
            console.log(target.health);
        };
    };

    this.faint = function() {
        console.log(`${this.name} fainted`);
    };

};
let bulbasaur = new Pokemon("bulbasaur", 16);
let rattata = new Pokemon("rattata", 8);